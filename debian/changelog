php-horde-imsp (2.0.10-6) unstable; urgency=medium

  * d/control: Add to Uploaders: Juri Grabowski.
  * d/control: Bump DH compat level to version 13.
  * d/salsa-ci.yml: Add file with salsa-ci.yml and pipeline-jobs.yml calls.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 01 Jul 2020 13:11:05 +0200

php-horde-imsp (2.0.10-5) unstable; urgency=medium

  * Re-upload to Debian. (Closes: #959331).

  * d/control: Add to Uploaders: Mike Gabriel.
  * d/control: Drop from Uploaders: Debian QA Group.
  * d/control: Bump Standards-Version: to 4.5.0. No changes needed.
  * d/control: Add Rules-Requires-Root: field and set it to 'no'.
  * d/upstream/metadata: Add file. Comply with DEP-12.
  * d/copyright: Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 21 May 2020 08:52:57 +0200

php-horde-imsp (2.0.10-4) unstable; urgency=medium

  * Bump debhelper from old 11 to 12.
  * d/control: Orphaning package (See #942282)

 -- Mathieu Parent <sathieu@debian.org>  Fri, 18 Oct 2019 20:25:07 +0200

php-horde-imsp (2.0.10-3) unstable; urgency=medium

  * Update Standards-Version to 4.1.4, no change
  * Update Maintainer field

 -- Mathieu Parent <sathieu@debian.org>  Tue, 15 May 2018 18:07:58 +0200

php-horde-imsp (2.0.10-2) unstable; urgency=medium

  * Update Standards-Version to 4.1.3, no change
  * Upgrade debhelper to compat 11
  * Update Vcs-* fields
  * Use secure copyright format URI
  * Replace "Priority: extra" by "Priority: optional"

 -- Mathieu Parent <sathieu@debian.org>  Fri, 06 Apr 2018 13:56:14 +0200

php-horde-imsp (2.0.10-1) unstable; urgency=medium

  * New upstream version 2.0.10

 -- Mathieu Parent <sathieu@debian.org>  Wed, 27 Sep 2017 22:25:35 +0200

php-horde-imsp (2.0.9-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.8, no change
  * Updated d/watch to use https

 -- Mathieu Parent <sathieu@debian.org>  Wed, 08 Jun 2016 08:42:36 +0200

php-horde-imsp (2.0.9-1) unstable; urgency=medium

  * New upstream version 2.0.9

 -- Mathieu Parent <sathieu@debian.org>  Sat, 26 Mar 2016 13:59:26 +0100

php-horde-imsp (2.0.8-3) unstable; urgency=medium

  * Update Standards-Version to 3.9.7, no change
  * Use secure Vcs-* fields
  * Rebuild with newer pkg-php-tools for the PHP 7 transition

 -- Mathieu Parent <sathieu@debian.org>  Sun, 13 Mar 2016 21:21:09 +0100

php-horde-imsp (2.0.8-2) unstable; urgency=medium

  * Upgaded to debhelper compat 9

 -- Mathieu Parent <sathieu@debian.org>  Sat, 24 Oct 2015 08:08:26 +0200

php-horde-imsp (2.0.8-1) unstable; urgency=medium

  * Update gbp.conf
  * New upstream version 2.0.8

 -- Mathieu Parent <sathieu@debian.org>  Mon, 10 Aug 2015 01:47:19 +0200

php-horde-imsp (2.0.7-1) unstable; urgency=medium

  * Update Standards-Version to 3.9.6, no change
  * New upstream version 2.0.7

 -- Mathieu Parent <sathieu@debian.org>  Mon, 04 May 2015 22:53:25 +0200

php-horde-imsp (2.0.5-2) unstable; urgency=medium

  * Update Standards-Version, no change
  * Update Vcs-Browser to use cgit instead of gitweb

 -- Mathieu Parent <sathieu@debian.org>  Tue, 26 Aug 2014 22:20:03 +0200

php-horde-imsp (2.0.5-1) unstable; urgency=low

  * New upstream version 2.0.4
  * New upstream version 2.0.5 (fixing lone block in tarball)

 -- Mathieu Parent <sathieu@debian.org>  Thu, 13 Jun 2013 18:55:13 +0200

php-horde-imsp (2.0.3-2) unstable; urgency=low

  * Use pristine-tar

 -- Mathieu Parent <sathieu@debian.org>  Thu, 06 Jun 2013 09:21:55 +0200

php-horde-imsp (2.0.3-1) unstable; urgency=low

  * New upstream version 2.0.3

 -- Mathieu Parent <sathieu@debian.org>  Sun, 07 Apr 2013 15:54:05 +0200

php-horde-imsp (2.0.2-1) unstable; urgency=low

  * New upstream version 2.0.2

 -- Mathieu Parent <sathieu@debian.org>  Thu, 10 Jan 2013 20:07:51 +0100

php-horde-imsp (2.0.1-2) unstable; urgency=low

  * Add a description of Horde in long description
  * Updated Standards-Version to 3.9.4, no changes
  * Replace horde4 by PEAR in git reporitory path
  * Fix Horde Homepage
  * Remove debian/pearrc, not needed with latest php-horde-role

 -- Mathieu Parent <sathieu@debian.org>  Wed, 09 Jan 2013 20:23:43 +0100

php-horde-imsp (2.0.1-1) unstable; urgency=low

  * Horde_Imsp package
  * Initial packaging (Closes: #695394)

 -- Mathieu Parent <sathieu@debian.org>  Sat, 08 Dec 2012 20:14:41 +0100
